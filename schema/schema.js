const fetch = require('node-fetch')
const util = require('util')
const parseXML = util.promisify(require('xml2js').parseString)
const {GraphQLSchema,GraphQLObjectType,GraphQLInt,GraphQLString} = require('graphql')

var tabs = [
    {author:{
           "id":4432,
           "name":"aymen"
      
    }},
    {author:{
        "id":7772,
        "name":"aymen"
   
 }},
    ]

const AuthorType = new GraphQLObjectType({
    name : 'Author',
    description:'...',

    fields: ()=>({

        name:{
             type:GraphQLString,
            resolve: xml =>  xml[0].author.name
             

        }
    })
})

module.exports = new GraphQLSchema({

    query : new GraphQLObjectType({
        name:"query",
        description:'...',
        fields: () => ({
          author: {
              type:AuthorType,
              args:{
                  id:{type:GraphQLInt}
              },
              resolve: (root,args)=> tabs.filter(x=>x.author.id === args.id)
          }

        })
    })
})
